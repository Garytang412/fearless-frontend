import React, { Component } from 'react'

export default class LocationForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            room_count: '',
            city: '',
            states: []
        };
    }

    async componentDidMount() {
        const url = 'http://localhost:8000/api/states/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ states: data.states });
        }
    }

    handleSubmit = async (e) => {
        e.preventDefault();
        const data = { ...this.state };
        delete data.states;

        // fetch:
        const locationUrl = "http://localhost:8000/api/locations/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const resp = await fetch(locationUrl, fetchConfig);
        if (resp.ok) {
            const newLocation = await resp.json();
            console.log(newLocation);
            this.setState({
                name: '',
                room_count: '',
                city: '',
                states: [],
            });
        }
        else { console.log("post respond error") }
    }







    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <h1>Create a new location</h1>
                            <form id="create-location-form" onSubmit={this.handleSubmit}>
                                <div className="form-floating mb-3">
                                    <input onChange={e => { this.setState({ name: e.target.value }) }} placeholder="Name" required type="text" name="name" id="name"
                                        className="form-control" value={this.state.name} />
                                    <label htmlFor="name">Name</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={(e) => { this.setState({ room_count: e.target.value }) }} placeholder="Room count" required type="number" name="room_count" id="room_count"
                                        className="form-control" value={this.state.room_count} />
                                    <label htmlFor="room_count">Room count</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={(e) => { this.setState({ city: e.target.value }) }} placeholder="City" required type="text" name="city" id="city"
                                        className="form-control" value={this.state.city} />
                                    <label htmlFor="city">City</label>
                                </div>
                                <div className="mb-3">
                                    <select required onChange={(e) => { this.setState({ state: e.target.value }) }} name="state" id="state" className="form-select">
                                        <option defaultValue="">Choose a state</option>
                                        {this.state.states.map(s => {
                                            return (<option key={s.abbreviation} value={s.abbreviation}>{s.name}</option>)
                                        })}
                                    </select>
                                </div>
                                <button className="btn btn-primary">Create</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
