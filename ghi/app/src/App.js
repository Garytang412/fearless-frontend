import Nav from "./Nav"
import AttendeesList from "./AttendeesList";
import LocationForm from "./LocationForm";
import ConferenceForm from "./ConferenceForm";



export default function App(props) {
  if (props.attendees === undefined) return null;
  return (
    <>
      <Nav />
      <div className="container text-center mt-3">
        {/* <LocationForm /> */}
        {/* <AttendeesList attendees={props.attendees} /> */}
        <ConferenceForm />
      </div>
    </>
  );
}

