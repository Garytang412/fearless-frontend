import React from 'react'

export default function AttendeesList(props) {
    return (
        <div>
            <table className='table table-hover table-striped'>
                <thead>
                    <tr className='table-primary'>
                        <th>Name</th>
                        <th>Conference</th>
                    </tr>
                </thead>
                <tbody>
                    {props.attendees.map(attendee => {
                        return (
                            <tr key={attendee.href} className="table-secondary">
                                <td>{attendee.name}</td>
                                <td>{attendee.conference}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}
