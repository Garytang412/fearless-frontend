import React, { Component } from 'react'

export default class ConferenceForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            starts: '',
            ends: '',
            description: '',
            max_presentations: '',
            max_attendees: '',
            location: '',
            locations: [],
        };
    }
    handleInputChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }
    async componentDidMount() {
        const locationListUrl = 'http://localhost:8000/api/locations/';
        try {
            const response = await fetch(locationListUrl);
            if (response.ok) {
                const locationData = await response.json();
                this.setState({ locations: locationData.locations });

            }
            else { console.log("location list response error!") }
        }
        catch (e) {
            console.log(e);
        }
    }

    handleSubmit = async (e) => {
        // 1. stop event jump:
        e.preventDefault();
        // 2. get FormData
        const formData = this.state;
        delete formData.locations;
        console.log(formData);
        // 3.  POST formData to http://localhost:8000/api/conferences/:
        const postUrl = 'http://localhost:8000/api/conferences/';
        try {
            // 4. fetch post:
            const response = await fetch(postUrl, {
                method: "post",
                body: JSON.stringify(formData),
                headers: {
                    "Content-Type": "application/json",
                },
            });
            if (response.ok) {
                const data = await response.json();
                console.log(data);
                this.setState({
                    name: '',
                    starts: '',
                    ends: '',
                    description: '',
                    max_presentations: '',
                    max_attendees: '',
                    location: '',
                    locations: [],
                })
            }
            else { console.log("post response error!") }

        } catch (e) {
            console.log(e);
        }




    }









    render() {

        return (<div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new conference</h1>
                        <form id="create-conference-form" onSubmit={this.handleSubmit}>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleInputChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name"
                                    className="form-control" />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleInputChange} value={this.state.starts} placeholder="Room count" required type="date" name="starts" id="starts"
                                    className="form-control" />
                                <label htmlFor="starts">starts</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleInputChange} value={this.state.ends} placeholder="Room count" required type="date" name="ends" id="ends"
                                    className="form-control" />
                                <label htmlFor="ends">ends</label>
                            </div>
                            <div className="form-floating mb-3">
                                <textarea onChange={this.handleInputChange} value={this.state.description} className="form-control" name="description" id="description" cols="30"
                                    rows="10"></textarea>
                                <label htmlFor="description">description</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleInputChange} value={this.state.max_presentations} placeholder="max_presentations" required type="number" name="max_presentations"
                                    id="max_presentations" className="form-control" />
                                <label htmlFor="max_presentations">max_presentations</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleInputChange} value={this.state.max_attendees} placeholder="attendees" required type="number" name="max_attendees"
                                    id="max_sentations" className="form-control" />
                                <label htmlFor="max_attendees">max_attendees</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleInputChange} required name="location" id="location" className="form-select" value={this.state.location}>
                                    <option value="">Choose a location</option>
                                    {this.state.locations.map(i => { return (<option key={i.id} value={i.id}>{i.name}</option>) })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>)
    }
}
