console.log('worked!')
const createOption = (name, abb) => {
    return `<option value="${abb}">${name}</option>`
}


window.addEventListener("DOMContentLoaded", async () => {

    // add states options:
    try {
        const response = await fetch("http://localhost:8000/api/states/");
        if (response.ok) {
            const data = await response.json();
            const stateList = data.states;
            // get target selectTag
            const selectTag = document.querySelector('#state');
            // add all options to select innerHtml
            stateList.forEach(state => {
                selectTag.innerHTML += createOption(state.name, state.abbreviation);
            })
        } else {
            console.log('Response Error!')
        }
    } catch (e) {
        console.log(e)
    }

    // submit handler:
    // 1. select target form
    const locationCreateForm = document.querySelector('#create-location-form');
    // 2. add eventlistener to target form
    locationCreateForm.addEventListener('submit', async (event) => {
        // 3. stop event jump:
        event.preventDefault();
        // 4. get FormData
        const formData = Object.fromEntries(new FormData(locationCreateForm));
        // 5.  POST formData to http://localhost:8000/api/locations/ :
        const postUrl = 'http://localhost:8000/api/locations/';
        try {
            // 6. fetch post:
            const response = await fetch(postUrl, {
                method: "post",
                body: JSON.stringify(formData),
                headers: {
                    "Content-Type": "application/json",
                },
            });
            if (response.ok) {
                locationCreateForm.reset();
                const data = await response.json();
                console.log(data)
            }
            else { console.log("post response error!") }

        } catch (e) {
            console.log(e);
        }





    });

});