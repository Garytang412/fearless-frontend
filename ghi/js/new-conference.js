window.addEventListener("DOMContentLoaded", async () => {
    const locationListUrl = 'http://localhost:8000/api/locations/';
    try {
        const response = await fetch(locationListUrl);
        if (response.ok) {
            const locationData = await response.json();
            const locationList = locationData.locations;
            const locationTag = document.querySelector('#location');
            locationList.forEach(i => {
                locationTag.innerHTML += `<option value="${i.id}">${i.name}</option>`;
            })
        }
        else { console.log("location list response error!") }
    }
    catch (e) {
        console.log(e);
    }


    // submit handler:
    // 1. select target form
    const conferenceCreateForm = document.querySelector('#create-conference-form');
    // 2. add eventlistener to target form
    conferenceCreateForm.addEventListener('submit', async (event) => {
        // 3. stop event jump:
        event.preventDefault();
        // 4. get FormData
        const formData = Object.fromEntries(new FormData(conferenceCreateForm));
        // 5.  POST formData to http://localhost:8000/api/conferences/:
        const postUrl = 'http://localhost:8000/api/conferences/';
        try {
            // 6. fetch post:
            const response = await fetch(postUrl, {
                method: "post",
                body: JSON.stringify(formData),
                headers: {
                    "Content-Type": "application/json",
                },
            });
            if (response.ok) {
                conferenceCreateForm.reset();
                const data = await response.json();
                console.log(data)
            }
            else { console.log("post response error!") }

        } catch (e) {
            console.log(e);
        }




    });
});