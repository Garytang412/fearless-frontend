

window.addEventListener("DOMContentLoaded", async () => {
    const conferenceListUrl = "http://localhost:8000/api/conferences/";
    try {
        const conferencesReq = await fetch(conferenceListUrl);
        if (conferencesReq.ok) {
            const conferenceData = await conferencesReq.json();
            const conferenceList = conferenceData.conferences;
            const conferenceTag = document.querySelector('#conference');
            conferenceList.forEach(i => {
                conferenceTag.innerHTML += `<option value="${i.href}">${i.name}</option>`
            });
            const spinTag = document.querySelector('#loading-conference-spinner');
            spinTag.classList.add('d-none');
            conferenceTag.classList.remove('d-none');
        }
        else { console.log("conference list request error!!") }

    } catch (e) { console.log(e) }

    // --- --- --- --- --- --*- ---- ---- --- ---
    //  select form
    const formTag = document.getElementById('create-attendee-form');
    // add event
    formTag.addEventListener('submit', async event => {
        // stop default event jump
        event.preventDefault();
        //  formdata
        const formData = Object.fromEntries(new FormData(formTag));
        //  fetchconfig
        const fetchConfig = {
            body: JSON.stringify(formData),
            headers: { "Content-Type": "application/json" },
            method: "post",
        };
        //  fetch post to http://localhost:8001/api/attendees/
        const attendeeUrl = "http://localhost:8001/api/attendees/";
        try {
            const response = await fetch(attendeeUrl, fetchConfig);
            if (response.ok) {
                //  add d-none to form, remove d-none from success
                formTag.classList.add('d-none');
                const successTag = document.getElementById('success-message');
                successTag.classList.remove('d-none');


            } else { console.log("attendee post response error!!") }
        } catch (e) { console.log(e) }

    })



});