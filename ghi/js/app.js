const createCard = (name, description, location, picture_url, starts, ends) => {
    return `<div class="col">
    <div class="card h-100 shadow">
      <img src="${picture_url}" class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer">
        <small class="text-muted">${starts}-${ends}</small>
      </div>
    </div>
    </div>`
}





window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);

        if (!response.ok) {
            // Figure out what to do when the response is bad
        }

        else {
            const data = await response.json();
            const conferences = data.conferences;
            const row = document.querySelector('.row');

            conferences.forEach(async (conference) => {
                const detailResponse = await fetch(`http://localhost:8000${conference.href}`);
                if (detailResponse.ok) {
                    const conferenceDetail = await detailResponse.json();
                    const starts = conferenceDetail.conference.starts.split('T')[0].replaceAll('-', '/');
                    const ends = conferenceDetail.conference.ends.split("T")[0].replaceAll('-', '/');
                    const title = conferenceDetail.conference.name;
                    const description = conferenceDetail.conference.description;
                    const location = conferenceDetail.conference.location.name;
                    const picture_url = conferenceDetail.conference.location.picture_url;
                    row.innerHTML +=
                        createCard(title, description, location, picture_url, starts, ends);
                }
            });
        }
    }

    catch (e) {
        // Figure out what to do if an error is raised
        console.log(e)
    }

});